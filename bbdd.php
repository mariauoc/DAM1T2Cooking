<?php
// Función que recibe un idchef y devuelve su categoría
function getCategoryById($idchef) {
    $c = conectar();
    $select = "select category from chef where idchef=$idchef";
    $resultado = mysqli_query($c, $select);
    $fila = mysqli_fetch_assoc($resultado);
    desconectar($c);
    return $fila["category"];
    // extract($fila);
    // return $category;
}


// Función que recibe idchef y code
// y comprueba que es correcto devuelve boolean
function verificarChef($idchef, $code) {
    $c = conectar();
    $select = "select idchef from chef where idchef=$idchef and code=$code";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
//    if (mysqli_num_rows($resultado) == 1) {
//        return true;
//    } else {
//        return false;
//    }
    // Sentencia equivalente al if anterior
    return mysqli_num_rows($resultado) == 1;
}

// Función que cierra una conexión
function desconectar($conexion) {
    mysqli_close($conexion);
}

// Función que se conecta a una base de datos (school)
function conectar() {
    // conectamos a la bbdd (nos devuelve una conexión)
    $conexion = mysqli_connect("localhost", "root", "root", "cooking");
    // si no ha podido conectar devuelve null, comprobamos
    if (!$conexion) {
        // Acabamos el programa dando msg de error
        die("No se ha podido establecer la conexión con el servidor");
    }
    // si todo ha ido ok devolvemos la conexión
    return $conexion;
}

