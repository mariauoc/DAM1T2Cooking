<?php
session_start();
require_once "bbdd.php";
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Home Page</title>
    </head>
    <body>
        <h1>Bienvenido a Cooking Manager</h1>
        <form method="POST">
            <p>Código de chef: <input type="number" name="chef"></p>
            <p>Código de acceso: <input type="password" name="code"></p>
            <input type="submit" name="login" value="Entrar">
        </form>
        <a href="registro.php">Registrarse</a>
        <?php
        if (isset($_POST["login"])) {
            $chef = $_POST["chef"];
            $code = $_POST["code"];
            if (verificarChef($chef, $code)) {
                // Guardamos el identificador del usuario en la sesión
                $_SESSION["chef"] = $chef;
                $tipo = getCategoryById($chef);
                // Guardamos el tipo de usuario en la sesión
                $_SESSION["tipo"]  = $tipo;
                if ($tipo == "Chef") {
                    header("Location: homechef.php");
                } else {
                    header("Location: homeboss.php");
                }
            } else {
                echo "Usuario o contraseña incorrecto";
            }
        }
        ?>
    </body>
</html>
